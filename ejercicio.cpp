#include <stdlib.h>
#include <stdio.h>

#define L 5

int main(int argc, char const *argv[])
{
    for (int i=0; i<L; i++){
        for (int j=0; j<L; j++)
        printf("*");
        printf("\n");
    }
       
    return EXIT_SUCCESS;
}
