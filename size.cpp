#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    int a;
    char nombre [] = "Paco";
    char *pnom = nombre;
    printf ("Tamaño de Paco: %lu\n", sizeof(nombre));
    printf ("Tamaño de puntero nombre: %lu\n\n", sizeof(pnom));

    printf ("Tamaño de a: \t%lu\n", sizeof(a));
    printf ("Tamaño de int: \t%lu\n", sizeof(int));
    printf ("a tiene basura: a: %i\n", a);

  printf("El minimo valor para un signed char = %i\n", SCHAR_MIN);
   printf("El maximo valor para un signed char = %i\n", SCHAR_MAX);
   printf("El maximo valor para un unsigned char = %i\n\n", UCHAR_MAX);

   printf("El minimo valor para un signed short int = %i\n", SHRT_MIN);
   printf("El maximo valor para un signed short int = %i\n", SHRT_MAX);
   printf("El maximo valor para un unsigned short int = %i\n\n", USHRT_MAX);

   printf("El minimo valor para un signed int = %i\n", INT_MIN);
   printf("El maximo valor para un signed int = %i\n", INT_MAX);
   printf("El maximo valor para un unsigned int = %u\n\n", UINT_MAX);

   printf("El minimo valor para un signed long int = %li\n", LONG_MIN);
   printf("El maximo valor para un signed long int = %li\n", LONG_MAX);
   printf("El maximo valor para un unsigned long int = %lu\n\n", ULONG_MAX);

   printf("El minimo valor para un signed long long int = %lli\n", LONG_LONG_MIN);
   printf("El maximo valor para un signed long long int = %lli\n", LONG_LONG_MAX);
   printf("El maximo valor para un unsigned long long int = %llu\n\n", ULONG_LONG_MAX);

   printf("%s\n",nombre);

   for (int i=0; i<strlen(nombre); i++)
   printf("%c ", pnom[i]);
   printf("\n");

return EXIT_SUCCESS;
}
