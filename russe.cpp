#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

/****************/
/*  ALGORITMO   */
/****************/
int multiplica (int num1, int num2) {
    int acumulador = 0;
    int ajustador = 1; 
    if (num1 < 0) {
        abs(num1);
        ajustador = -ajustador;
    }  

    while (num1 != 0){
        if (num1 %2 != 0)
        acumulador += num2;
        num1 = num1 / 2;
        num2 = num2 * 2;
    }
    return acumulador * ajustador;
}

/*************/
/*   TESTS   */
/*************/
TEST_SUITE("Multiplicacion") {

  TEST_CASE("Criterio de signos") {
      CHECK_EQ(4, multiplica(2, 2));
      CHECK_EQ(-35, multiplica(-5, 7));
      CHECK_EQ(-35, multiplica(5, -7));
      CHECK_EQ(35, multiplica(-5,-7));
      CHECK_EQ(0, multiplica(0,0));
      
  }
}
