#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

int suma (int op1, int op2){
    return op1 +op2;    
}
int multiplicacion (int op1, int op2){
    return op1 * op2;
}

TEST_SUITE("Calculadora"){

    TEST_CASE("Naturales"){
        CHECK_EQ(4, suma(2,2));
        CHECK_EQ(12, suma(7,5));
        CHECK_EQ(-12, suma(-7,-5));
    }
    TEST_CASE("Enteros"){
        CHECK_EQ(-12, suma(-7,-5));
    }
    TEST_CASE("Multiplicación"){
        CHECK_EQ(-35, multiplicacion(7,-5));
        CHECK_EQ(-35, multiplicacion(7,5));
        CHECK_EQ(-35, multiplicacion(7,5));
    }
}