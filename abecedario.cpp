#include <stdio.h>
#include <stdlib.h>

int main() {
    for(char caracter = 'a'; caracter <= 'z'; caracter++) {
        printf("ASCII value: %i, char value: %c \n", caracter, caracter);
        if(caracter == 'n') {
            char *ene = "ñ";
            printf("Whatever value: %i, char value: %s \n", ene, ene);
        }
    }
    return 0;
}